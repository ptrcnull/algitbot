#!/usr/bin/lua5.3

local sircbot  = require("sircbot")
local cjson    = require("cjson")

local topic      = arg[1]
local payload    = arg[2]
local projects   = os.getenv("MQTT_EXEC_PROJECTS")
local commitchan = os.getenv("MQTT_EXEC_COMMIT_CHANNEL") or "#alpine-commits"
local infrachan  = os.getenv("MQTT_EXEC_INFRA_CHANNEL") or "#alpine-infra"

local function list_has(list, item)
	for i in list:gmatch("%S+") do
		if i == item then return true end
	end
	return false
end

-- exclude projects not in alpine root
local function alpine_project(p)
	local res = p:match("^alpine/(.*)")
	return (res and not res:match("/")) and true or false
end

-- convert color to irc value
-- arguments: message, color, format
local function cc(m,c,f)
	local format = (f == "bold") and "\x02" or ""
	local color = c or "white"
	local code = {
		white = "00", black = "01", blue = "02", green = "03", red = "04",
		brown = "05", purple = "06", orange = "07", yellow = "08", lime = "09",
		teal = "10", aqua = "11", royal = "12", pink = "13", grey = "14",
		silver = "15"
	}
	return ("%s\x03%s%s\x0F"):format(format, code[color], m)
end

-- convert status to color
local function sc(s)
	local t = { problem = "red", solved = "green", ack = "blue" }
	return type(t[s]) and t[s] or "grey"
end

-- convert severity id to color
local function svc(s)
	local t = { Information = "blue", Warning = "pink",
		Average = "purple", High = "orange", Disaster = "red" }
	return type(t[s]) and t[s] or "grey"
end

-- some messages are emtpy
if payload == nil then
	os.exit(0)
end

-- announce commits
if topic:match("^gitlab/push/.*") then
	local data = cjson.decode(payload)
	if alpine_project(data.project.path_with_namespace) or
		list_has(projects, data.project.path_with_namespace) then
		for _,commit in ipairs(data.commits) do
			local msg = ("%s:%s | %s | %s | http://dup.pw/%s/%s"):format(
				cc(data.project.path_with_namespace, "white", "bold"),
				data.ref:match("^refs/heads/(.*)"),
				commit.author.name,
				commit.message:match("^(%C+)"),
				data.project.path_with_namespace,
				commit.id:sub(1,8)
			)
			sircbot.connect(commitchan):send(msg)
		end
		if tonumber(data.total_commits_count) > 20 then
			local msg = ("Limiting output to 20 commits out of %s."):format(
				tostring(data.total_commits_count))
			sircbot.connect(commitchan):send(msg)
		end
	end
end

-- anounce build errors
if topic:match("^build/.*/errors") then
	local data = cjson.decode(payload)
	local host = data.hostname or string.match(topic, "build/(.*)/errors")
	local msg = ("%s: failed to build %s: %s"):format(
		cc(host, "red"), data.pkgname, data.logurl)
	sircbot.connect(commitchan):send(msg)
end

-- announce uploads
if topic:match("^rsync/") then
	local msg = ("%s: uploaded"):format(cc(payload, "green"))
	sircbot.connect(commitchan):send(msg)
end

-- alerts send by zabbix
if topic:match("^alert/infra") then
	local data = cjson.decode(payload)
	local host = cc(data.host, sc(data.status))
	local status = cc(("[%s]"):format(data.status), svc(data.severity), "bold")
	local msg = ("%s: %s | %s | http://dup.pw/a/%s/%s"):format(host, status,
		data.message, data.triggerid, data.eventid)
	sircbot.connect(infrachan):send(msg)
end

-- announce dns changes
if topic:match("^infra/dns") then
    local data = cjson.decode(payload)
    local color = data.status == "ok" and "green" or "red"
    local prefix = cc(("[%s][%s]:"):format("Infra", "DNS"), color)
    local msg = ("%s %s"):format(prefix, data.message)
    sircbot.connect(infrachan):send(msg)
end
